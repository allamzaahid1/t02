public class LCD {
      
    private String Status;
    private int Volume;
    private int Brightness;
    private String Cable;

    public void turnOff(){
        this.Status = "Off";
    }

    public void turnOn(){
        this.Status = "On";
    }

    public void Freeze(){
        this.Status = "Freeze";
    }

    public int volumeUp(){
        return this.Volume++;
    }

    public int volumeDown(){
        return this.Volume--;
    }

    public int setVolume(int volume){
        return this.Volume = volume;
    }

    public int brightnessUp(){
        return this.Brightness++;
    }

    public int brightnessDown(){
        return this.Brightness--;
    }

    public int setBrightness(int brightness){
        return this.Brightness = brightness;
    }

    public void cableUp(){
        if (this.Cable == "VGA") {
            this.Cable = "DVI";
        }
        if (this.Cable == "DVI") {
            this.Cable = "HDMI";
        }
        if (this.Cable == "HDMI") {
            System.out.println("Peringatan");
            System.out.println("Tidak dapat menaikkan kabel lebih jauh. Kabel dikembalikan ke HDMI");
            System.out.println("Kode Permasalahan = KabelNaik");
            this.Cable = "HDMI";
        }
    }

    public void cableDown(){
        if (this.Cable == "VGA") {
            System.out.println("Peringatan!");
            System.out.println("Tidak dapat menurunkan kabel lebih jauh. Kabel dikembalikan ke VGA");
            System.out.println("Kode Permasalahan = KabelTurun");
            this.Cable = "VGA";
        }
        if (this.Cable == "DVI") {
            this.Cable = "VGA";
        }
        if (this.Cable == "HDMI") {
            this.Cable = "DVI";
        }
    }

    public String setCable(String cable){
        if (cable == "VGA") {
            this.Cable = "VGA";
        }
        if (cable == "DVI") {
            this.Cable = "DVI";
        }
        if (cable == "HDMI") {
            this.Cable = "HDMI";
        }
        else {
            System.out.println("Peringatan!");
            System.out.println("Jenis Kabel yang tersedia hanyalah VGA, DVI, dan HDMI. Kabel akan secara otomatis dialihkan ke HDMI");
            System.out.println("Kode Permasalahan = Kabel");
            this.Cable = "HDMI";
        }
        return this.Cable;
    }

    public void Display(){
        System.out.println("-------------- LCD ----------------");
        System.out.println("Status LCD Saat Ini\t = "+this.Status);
        System.out.println("Volume LCD Saat Ini\t = "+this.Volume);
        System.out.println("Brightness Saat Ini\t = "+this.Brightness);
        System.out.println("Cable yang digunakan\t = "+this.Cable);
        System.out.println("-----------------------------------");
    }

}
