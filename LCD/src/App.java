public class App {
    public static void main(String[] args) {
        
        LCD lcd1 = new LCD();
        lcd1.turnOn();
        lcd1.setVolume(50);
        lcd1.setBrightness(75);
        lcd1.setCable("HD");
        lcd1.Display();

        System.out.println("\nKabel, Brightness, dan Volume dinaikan");
        System.out.println();
        lcd1.cableUp();
        lcd1.brightnessUp();
        lcd1.volumeUp();
        lcd1.Display();
        System.out.println();

        System.out.println("\nKabel, Brightness, dan Volume diturunkan");
        System.out.println();
        lcd1.cableDown();
        lcd1.brightnessDown();
        lcd1.volumeDown();
        lcd1.Display();
        System.out.println();

        System.out.println("\nKabel, Brightness, dan Volume diturunkan");
        System.out.println();
        lcd1.cableDown();
        lcd1.brightnessDown();
        lcd1.volumeDown();
        lcd1.Display();
        System.out.println();

        System.out.println("\nKabel, Brightness, dan Volume diturunkan");
        System.out.println();
        lcd1.cableDown();
        lcd1.brightnessDown();
        lcd1.volumeDown();
        lcd1.Display();
        System.out.println();
    }
}
